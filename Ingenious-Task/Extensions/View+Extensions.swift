//
//  View+Extensions.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 23/01/2024.
//

import SwiftUI

extension View {

  // MARK: - Public

  func errorAlert(error: Binding<Error?>, buttonTitle: String = "OK") -> some View {
    let isPresented = Binding<Bool>(
      get: { error.wrappedValue != nil },
      set: { _ in error.wrappedValue = nil }
    )
    return alert(Text(error.wrappedValue?.localizedDescription ?? ""), isPresented: isPresented) {
      Button(buttonTitle) {
        error.wrappedValue = nil
      }
    }
  }
}
