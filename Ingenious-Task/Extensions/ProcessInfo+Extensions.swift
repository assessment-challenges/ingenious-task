//
//  ProcessInfo+Extensions.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation

extension ProcessInfo {

  // MARK: - Properties

  var canProccedRunningApplication: Bool {
    !isRunningUnitTests
  }

  private var isRunningUnitTests: Bool {
    environment["XCTestConfigurationFilePath"] != nil
  }
}
