//
//  ApplicationCoordinator.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation

protocol ApplicationCoordinatorType: ObservableObject {
  associatedtype UserListCoordinator: EntryCoordinatorType
  var userListCoordinator: UserListCoordinator? { get }
}

final class ApplicationCoordinator: ApplicationCoordinatorType {

  // MARK: - Properties

  @Published private(set) var userListCoordinator: EntryCoordinator?

  private let apiService: any ApiService

  // MARK: - Initialization

  init(apiService: any ApiService) {
    self.apiService = apiService
    self.userListCoordinator = UserListCoordinator(apiService: apiService)
  }
}
