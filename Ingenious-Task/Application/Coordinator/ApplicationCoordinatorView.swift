//
//  ApplicationCoordinatorView.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import SwiftUI

struct ApplicationCoordinatorView<Coordinator: ApplicationCoordinatorType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var coordinator: Coordinator

  // MARK: - Views

  var body: some View {
    NavigationSplitView {
      if let userListCoordinator = coordinator.userListCoordinator {
        EntryCoordinatorView(coordinator: userListCoordinator)
      }
    } detail: {
      Text("list_split_details")
    }
  }
}
