//
//  Ingenious_TaskApp.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import SwiftUI

@main
struct IngeniousTaskApp: App {

  // MARK: - Properties

  @StateObject var coordinator = ApplicationCoordinator(apiService: GithubService())

  // MARK: - Views

  var body: some Scene {
    WindowGroup {
      if ProcessInfo.processInfo.canProccedRunningApplication {
        ApplicationCoordinatorView(coordinator: coordinator)
      }
    }
  }
}
