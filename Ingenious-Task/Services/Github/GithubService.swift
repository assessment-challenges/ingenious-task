//
//  GithubService.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation

final class GithubService: ApiService {

  // MARK: - Properties

  let base = URL(string: "https://api.github.com")

  // MARK: - Public

  func listOfUsers(since userId: Int) async throws -> [User] {
    try await fetchData(
      forURL: base?.urlListUsers(since: userId)
    )
  }

  func listOfUsers(query: String) async throws -> UsersEnvelope {
    try await fetchData(
      forURL: base?.urlSearchListUsers(query: query)
    )
  }

  func userDetails(login: String) async throws -> UserDetails {
    try await fetchData(
      forURL: base?.urlUserDetails(login: login)
    )
  }
}

private extension URL {

  // MARK: - Public

  func urlListUsers(since userId: Int) -> URL {
    let queryItems = [URLQueryItem(name: "since", value: "\(userId)")]
    return appendingPathComponent("/users").appending(queryItems: queryItems)
  }

  func urlUserDetails(login: String) -> URL {
    return appendingPathComponent("/users/\(login)")
  }

  func urlSearchListUsers(query: String) -> URL {
    let queryItems = [URLQueryItem(name: "q", value: "\(query)")]
    return appendingPathComponent("/search/users").appending(queryItems: queryItems)
  }
}
