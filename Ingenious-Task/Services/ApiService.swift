//
//  ApiService.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation

enum ApiSericeError: Error {
  case invalidUrl
  case networkError(Error)
}

protocol ApiService {
  func listOfUsers(since userId: Int) async throws -> [User]
  func listOfUsers(query: String) async throws -> UsersEnvelope
  func userDetails(login: String) async throws -> UserDetails
}

extension ApiService {

  // MARK: - Public

  func fetchData<T: Decodable>(forURL url: URL?) async throws -> T {
    guard let url else {
      throw ApiSericeError.invalidUrl
    }
    let data = try await fetchData(url: url)
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    return try decoder.decode(T.self, from: data)
  }

  // MARK: - Private

  private func fetchData(url: URL) async throws -> Data {
    do {
      let (data, _) = try await URLSession.shared.data(from: url)
      return data
    } catch {
      // This catch block is here to catch and handle network issues
      throw ApiSericeError.networkError(error)
    }
  }
}
