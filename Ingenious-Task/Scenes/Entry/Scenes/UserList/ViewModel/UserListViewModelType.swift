//
//  UserListViewModelType.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Combine
import Foundation

protocol UserListViewModelType: ObservableObject {
  associatedtype SchedulerType: Scheduler
  var users: [User] { get set }
  var selectedUser: User? { get set }
  var queryUsers: String { get set }
  var error: Error? { get set }
  init(apiService: ApiService, scheduler: SchedulerType)
  func onLastElementAppear(user: User)
  func onAppear()
}

final class UserListViewModel<S: Scheduler>: UserListViewModelType {

  // MARK: - Properties

  @Published var users: [User] = []
  @Published var selectedUser: User?
  @Published var queryUsers: String = ""
  @Published var error: Error?

  private let apiService: ApiService
  private let scheduler: S
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init(apiService: ApiService, scheduler: S) {
    self.apiService = apiService
    self.scheduler = scheduler
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    $queryUsers
      .dropFirst()
      .removeDuplicates()
      .filter { !$0.isEmpty }
      .debounce(for: .seconds(0.5), scheduler: scheduler)
      .sink(receiveValue: { [weak self] query in
        self?.fetchUsers(with: query)
      })
      .store(in: &cancellables)

    $queryUsers
      .dropFirst()
      .removeDuplicates()
      .filter { $0.isEmpty }
      .sink(receiveValue: { [weak self] _ in
        self?.fetchUsersList()
      })
      .store(in: &cancellables)
  }

  // MARK: - Public

  func onAppear() {
    guard selectedUser == nil else { return }
    fetchUsersList()
  }

  func onLastElementAppear(user: User) {
    guard users.last == user, queryUsers.isEmpty else { return }
    fetchUsersList(since: user.id, append: true)
  }

  // MARK: - Private

  private func fetchUsersList(since userId: Int = 0, append: Bool = false) {
    fetchUsers(append: append) {
      try await self.apiService.listOfUsers(since: userId)
    }
  }

  private func fetchUsers(with query: String) {
    fetchUsers {
      try await self.apiService.listOfUsers(query: query).items
    }
  }

  private func fetchUsers(append: Bool = false, function: @escaping () async throws -> [User]) {
    Task { @MainActor in
      do {
        let newUsers = try await function()
        if append {
          users.append(contentsOf: newUsers)
        } else {
          users = newUsers
        }
      } catch {
        self.error = error
      }
    }
  }
}
