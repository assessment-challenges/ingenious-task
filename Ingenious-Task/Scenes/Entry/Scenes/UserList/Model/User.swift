//
//  User.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation

struct User: Codable, Identifiable, Equatable {

  // MARK: - Properties

  let login: String
  let id: Int
  let avatarUrl: String
}
