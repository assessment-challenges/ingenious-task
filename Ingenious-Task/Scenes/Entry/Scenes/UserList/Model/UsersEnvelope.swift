//
//  UsersEnvelope.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 24/01/2024.
//

import Foundation

struct UsersEnvelope: Codable {

  // MARK: - Properties

  let items: [User]

  // MARK: - Initialization

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    let users = try container.decode([User].self, forKey: .items)
    self.items = users
  }

  init(items: [User]) {
    self.items = items
  }
}
