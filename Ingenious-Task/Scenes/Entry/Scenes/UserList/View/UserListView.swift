//
//  UserListView.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import SwiftUI

struct UserListView<ViewModel: UserListViewModelType>: View {

  @Environment(\.horizontalSizeClass) private var horizontalSizeClass

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    GeometryReader { proxy in
      ScrollView {
        LazyVStack(alignment: .leading) {
          ForEach(viewModel.users) { user in
            UserRowView(user: user)
              .frame(maxWidth: .infinity, alignment: .leading)
              .contentShape(Rectangle())
              .onTapGesture(perform: {
                viewModel.selectedUser = user
              })
              .onAppear(perform: {
                viewModel.onLastElementAppear(user: user)
              })
            Divider()
          }
        }
      }
      .toolbar(content: {
        searchView.frame(width: proxy.size.width)
      })
      .onAppear(perform: {
        viewModel.onAppear()
      })
      .errorAlert(error: $viewModel.error)
    }
  }

  private var searchView: some View {
    TextField("search_field", text: $viewModel.queryUsers)
      .textFieldStyle(RoundedBorderTextFieldStyle())
      .padding()
  }
}

private struct UserRowView: View {

  // MARK: - Properties

  var user: User

  // MARK: - Views

  var body: some View {
    HStack {
      AsyncImage(url: URL(string: user.avatarUrl)) { image in
        image
          .resizable()
          .aspectRatio(contentMode: .fit)
          .frame(width: 40, height: 40)
          .clipShape(Circle())
      } placeholder: {
        ProgressView()
          .frame(width: 40, height: 40)
      }

      VStack(alignment: .leading) {
        Text(user.login)
          .font(.headline)
        Text("Id: \(user.id)")
          .font(.subheadline)
      }
    }
    .padding(8)
  }
}
