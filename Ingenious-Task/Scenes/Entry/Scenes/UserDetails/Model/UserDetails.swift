//
//  UserDetails.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 23/01/2024.
//

import Foundation

struct UserDetails: Codable, Identifiable {

  // MARK: - Properties

  let login: String
  let id: Int
  let avatarUrl: String
  let location: String?
  let name: String
}
