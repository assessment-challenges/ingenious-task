//
//  UserDetailsView.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 23/01/2024.
//

import SwiftUI

struct UserDetailsView<ViewModel: UserDetailsViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    VStack {
      Text(viewModel.userDetails?.name ?? "")
        .font(.title)
        .fontWeight(.bold)

      Text(viewModel.userDetails?.login ?? "")
        .font(.subheadline)
        .foregroundColor(.gray)

      AsyncImage(url: URL(string: viewModel.userDetails?.avatarUrl ?? "")) { image in
        image
          .resizable()
          .aspectRatio(contentMode: .fit)
          .frame(width: 100, height: 100)
          .clipShape(Circle())
      } placeholder: {
        ProgressView()
          .frame(width: 100, height: 100)
      }

      Text(viewModel.userDetails?.location ?? "")
        .font(.body)

      Spacer()
    }
    .padding()
    .errorAlert(error: $viewModel.error)
  }
}
