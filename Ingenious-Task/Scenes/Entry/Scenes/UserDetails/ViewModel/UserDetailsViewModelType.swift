//
//  UserDetailsViewModelType.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 23/01/2024.
//

import Foundation

protocol UserDetailsViewModelType: ObservableObject, Hashable {
  var userDetails: UserDetails? { get set }
  var error: Error? { get set }
  init(apiService: ApiService, login: String)
}

final class UserDetailsViewModel: UserDetailsViewModelType {

  static func == (lhs: UserDetailsViewModel, rhs: UserDetailsViewModel) -> Bool {
    lhs.login == rhs.login
  }

  // MARK: - Properties

  @Published var userDetails: UserDetails?
  @Published var error: Error?

  private let login: String
  private let apiService: ApiService

  // MARK: - Initialization

  init(apiService: ApiService, login: String) {
    self.apiService = apiService
    self.login = login
    self.fetchUserDetails()
  }

  // MARK: - Public

  func hash(into hasher: inout Hasher) {
    hasher.combine(login)
  }

  // MARK: - Private

  private func fetchUserDetails() {
    Task { @MainActor in
      do {
        userDetails = try await apiService.userDetails(login: login)
      } catch {
        self.error = error
      }
    }
  }
}
