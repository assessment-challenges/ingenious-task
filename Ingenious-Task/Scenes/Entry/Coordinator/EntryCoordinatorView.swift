//
//  UserListCoordinatorView.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import SwiftUI

struct EntryCoordinatorView<Coordinator: EntryCoordinatorType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var coordinator: Coordinator

  // MARK: - Views

  var body: some View {
    if let userListViewModel = coordinator.userListViewModel {
      UserListView(viewModel: userListViewModel)
        .navigationDestination(item: $coordinator.userDetailsViewModel) { userDetails in
          UserDetailsView(viewModel: userDetails)
        }
    }
  }
}
