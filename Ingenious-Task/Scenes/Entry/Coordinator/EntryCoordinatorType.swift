//
//  ListCoordinatorType.swift
//  Ingenious-Task
//
//  Created by Patryk Grabowski on 22/01/2024.
//

import Foundation
import Combine

protocol EntryCoordinatorType: ObservableObject {
  associatedtype UserListViewModel: UserListViewModelType
  associatedtype UserDetailsViewModel: UserDetailsViewModelType

  var userListViewModel: UserListViewModel? { get set }
  var userDetailsViewModel: UserDetailsViewModel? { get set }
}

final class EntryCoordinator: EntryCoordinatorType {

  private enum State {
    case list
    case details(login: String)
  }

  // MARK: - Properties

  @Published var userListViewModel: UserListViewModel<RunLoop>?
  @Published var userDetailsViewModel: UserDetailsViewModel?

  private let apiService: any ApiService
  private let stateSubject: CurrentValueSubject<EntryCoordinator.State, Never>
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init(apiService: any ApiService) {
    self.apiService = apiService
    self.stateSubject = CurrentValueSubject(.list)
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    stateSubject
      .sink { [weak self] state in
        self?.setupViewModel(for: state)
      }
      .store(in: &cancellables)

    $userDetailsViewModel
      .filter { $0 == nil }
      .map { _ in State.list }
      .subscribe(AnySubscriber(stateSubject))
  }

  // MARK: - Private

  private func setupViewModel(for state: EntryCoordinator.State) {
    switch state {
      case .list where userListViewModel == nil:
        userListViewModel = prepareUserListViewModel()
      case .details(let login):
        userDetailsViewModel = prepareUserDetailsViewModel(login: login)
      default:
        break
    }
  }

  private func prepareUserListViewModel() -> UserListViewModel<RunLoop> {
    let viewModel = UserListViewModel(apiService: apiService, scheduler: .main)
    viewModel
      .$selectedUser
      .compactMap { $0?.login }
      .map { State.details(login: $0) }
      .subscribe(AnySubscriber(stateSubject))

    return viewModel
  }

  private func prepareUserDetailsViewModel(login: String) -> UserDetailsViewModel {
    UserDetailsViewModel(apiService: apiService, login: login)
  }
}
