# Ingenious Task

Develop a functional mobile application for both phones and tablets that displays a list of GitHub users (referred to as items) and their details. The user list should appear following a search, which is also a key feature of the application.

# Requirements:
- language: Swift
- architecture: MVVM (with coordinators here)
- multilingual support: English and Polish
- search functionality: Complete freedom in implementation 

List After Search:
The application should rely on API data, but the data source should be easily interchangeable.

API:

https://docs.github.com/en/rest

Key Points:
Navigation between views with appropriate titles.

Data access should occur on the correct threads, adhering to standards.

Additional Tasks:
~~Simple caching of the entire list and previously visited details.~~

The application should check for an internet connection before making a query and skip it if there is no chance of retrieving data from an external source.

## Project Overview

The project aims to deliver a highly extensible solution, adhering to the SOLID principles. It features a modular architecture, with services like GithubService designed for effortless substitution, providing flexibility and adaptability.

The implementation strictly follows SOLID principles, ensuring maintainability and scalability (also simple unit tests demo, exemplified by the **UserListViewModelTests**).

Utilizing the MVVM architecture along with coordinators, the application is constructed with SwiftUI, Combine, and Swift Concurrency for optimal thread management. The use of SwiftUI enables showing views navigation view (for iPhone devices) and in split view (for iPad devices)

Localization is achieved through a contemporary string catalog.

The goal was to implement solution which is easy expandable. Services such as (GithubService) coulb easy changed to different serive. Implementation fallow SOLID rules.

## Errors handling

To address the essential requirement of checking for an internet connection before making API requests, the project introduces ApiSericeError. This elegant error type, encompassing scenarios like invalid URLs and network errors, is rigorously tested. A seamless connection check is embedded within the error handling mechanism, ensuring graceful handling of potential network unavailability.

```
enum ApiSericeError: Error {
  case invalidUrl
  case networkError(Error)
}
```

