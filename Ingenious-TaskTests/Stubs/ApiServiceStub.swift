//
//  ApiServiceStub.swift
//  Ingenious-TaskTests
//
//  Created by Patryk Grabowski on 24/01/2024.
//

import XCTest
@testable import Ingenious_Task

final class ApiServiceStub: ApiService {

  // MARK: - Properties

  var users: [User]?
  var usersEnvelope: UsersEnvelope?
  var userDetails: UserDetails?

  // MARK: - Public

  func listOfUsers(since userId: Int) async throws -> [User] {
    guard let users else {
      throw StubErrorType.valueNotFound
    }
    return users
  }

  func listOfUsers(query: String) async throws -> UsersEnvelope {
    guard let usersEnvelope = usersEnvelope else {
      throw StubErrorType.valueNotFound
    }
    return usersEnvelope
  }

  func userDetails(login: String) async throws -> UserDetails {
    guard let userDetails else {
      throw StubErrorType.valueNotFound
    }
    return userDetails
  }
}
