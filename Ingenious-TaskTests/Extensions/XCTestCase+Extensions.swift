//
//  XCTestCase+Extensions.swift
//  Ingenious-TaskTests
//
//  Created by Patryk Grabowski on 24/01/2024.
//

import XCTest
import Combine

extension XCTestCase {

  // MARK: - Public

  func expectation<T, Failure: Error>(
    publisher: AnyPublisher<T, Failure>,
    expectedCount: Int,
    when: () -> Void,
    then: @escaping ([T]) -> Void,
    thenError: ((Error?) -> Void)? = nil,
    method name: String = #function) {
      let expectation = XCTestExpectation(description: name)
      if expectedCount == 0 {
        expectation.isInverted = true
      } else {
        expectation.expectedFulfillmentCount = expectedCount
      }
      var cancellables = Set<AnyCancellable>()
      var results = [T]()

      publisher
        .sink(
          receiveCompletion: { completion in
            guard case .failure(let error) = completion else { return }
            thenError?(error)
          },
          receiveValue: { value in
            results.append(value)
            expectation.fulfill()
          })
        .store(in: &cancellables)

      when()
      wait(for: [expectation], timeout: 1.0)
      then(results)
    }

  func expectation<T>(
    publisher: Published<T>.Publisher,
    expectedCount: Int,
    when: () -> Void,
    then: @escaping ([T]) -> Void,
    method name: String = #function) {
      expectation(
        publisher: publisher.eraseToAnyPublisher(),
        expectedCount: expectedCount,
        when: when,
        then: then,
        method: name
      )
    }
}

enum StubErrorType: Error {
  case valueNotFound
}
