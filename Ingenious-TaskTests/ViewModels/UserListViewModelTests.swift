//
//  UserListViewModelTests.swift
//  Ingenious-TaskTests
//
//  Created by Patryk Grabowski on 24/01/2024.
//

import XCTest
import Combine
@testable import Ingenious_Task

final class UserListViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: UserListViewModel<ImmediateScheduler>!
  private var apiServiceStub: ApiServiceStub!

  override func setUp() {
    super.setUp()
    apiServiceStub = ApiServiceStub()
    sut = UserListViewModel(apiService: apiServiceStub, scheduler: .shared)
  }

  override func tearDown() {
    sut = nil
    apiServiceStub = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testOnAppear() {
    // Given
    let users = [
      User(login: "testUser1", id: 1, avatarUrl: "testUrl1.com"),
      User(login: "testUser2", id: 2, avatarUrl: "testUrl2.com"),
      User(login: "testUser3", id: 3, avatarUrl: "testUrl3.com")
    ]
    apiServiceStub.users = users

    expectation(
      publisher: sut.$users,
      expectedCount: 2,
      when: { [weak self] in
        self?.sut.onAppear()
      },
      then: { [weak self] result in
        XCTAssertEqual(result.last, self?.sut.users)
      }
    )
  }

  func testOnLastElementAppear() {
    // Given
    let user = User(login: "testUser0", id: 0, avatarUrl: "testUrl0.com")
    let users = [
      User(login: "testUser1", id: 1, avatarUrl: "testUrl1.com"),
      User(login: "testUser2", id: 2, avatarUrl: "testUrl2.com"),
      User(login: "testUser3", id: 3, avatarUrl: "testUrl3.com")
    ]
    apiServiceStub.users = users
    sut.users = [user]
    let expected = [user] + users

    expectation(
      publisher: sut.$users,
      expectedCount: 2,
      when: { [weak self] in
        self?.sut.onLastElementAppear(user: user)
      },
      then: { [weak self] result in
        XCTAssertEqual(result.last, self?.sut.users)
        XCTAssertEqual(expected, self?.sut.users)
      }
    )
  }

  func testOnUserSelection() {
    // Given
    let user = User(login: "testUser", id: 1, avatarUrl: "testUrl.com")

    expectation(
      publisher: sut.$selectedUser,
      expectedCount: 2,
      when: { [weak self] in
        self?.sut.selectedUser = user
      },
      then: { results in
        XCTAssertEqual(results.last, user)
      }
    )
  }

  func testShouldFetchUsersQuery() {
    // Given
    let user = User(login: "testUser0", id: 0, avatarUrl: "testUrl0.com")
    let users = [
      User(login: "testUser", id: 1, avatarUrl: "testUrl.com"),
      User(login: "testUser2", id: 2, avatarUrl: "testUrl2.com"),
      User(login: "testUser3", id: 3, avatarUrl: "testUrl3.com")
    ]

    apiServiceStub.users = [user]
    apiServiceStub.usersEnvelope = UsersEnvelope(items: users)
    let expected = users

    expectation(
      publisher: sut.$users,
      expectedCount: 2,
      when: { [weak self] in
        self?.sut.queryUsers = "not empty string"
      },
      then: { [weak self] result in
        XCTAssertEqual(result.last, self?.sut.users)
        XCTAssertEqual(expected, self?.sut.users)
      }
    )
  }
}
